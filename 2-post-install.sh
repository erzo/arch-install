#!/usr/bin/env bash

set -euo pipefail
IFS="$(printf '\n\t')"

. ./ui.sh


# Settings
# ========

if [ -f settings2 ]; then
	vim settings2
	. ./settings2
elif ! ask_yes_no "Failed to find settings2. Do you want to proceed? "; then
	exit
fi

SWAY="${SWAY:-y}"
X11="${X11:-n}"
GNOME="${GNOME:-n}"
LAPTOP="${LAPTOP:-y}"
USER_SOFTWARE="${USER_SOFTWARE:-y}"
DEVELOPMENT_TOOLS="${DEVELOPMENT_TOOLS:-y}"
RUST="${RUST:-y}"
NEOVIM="${NEOVIM:-y}"
PASS="${PASS:-y}"
COPY_PASSWORDS="${COPY_PASSWORDS:-n}"
NEW_PASS_KEY="${NEW_PASS_KEY:-}"
DOTFILES="${DOTFILES:-}"
AUR="${AUR:-y}"
YAY="${YAY:-n}"
SUDO="${SUDO:-y}"
NEOMUTT="${NEOMUTT:-y}"
JAPANESE_INPUT="${JAPANESE_INPUT:-y}"
LATEX="${LATEX:-y}"


# Enable System Request Key
# =========================
# this defaults to 16 (sync only), see /usr/lib/sysctl.d/50-default.conf
# https://www.kernel.org/doc/html/latest/admin-guide/sysrq.html

cat >/usr/lib/sysctl.d/99-magic-sysrq.conf <<EOF
kernel.sysrq = 1
EOF


# Enable Network Manager
# ======================

systemctl start NetworkManager
systemctl enable NetworkManager

pacinst() {
	echo pacman -S --noconfirm "$@"
	pacman -S --noconfirm "$@"
}

while ! has_internet; do
	echo "waiting for internet"
	sleep 1
done


# Enable Time Synchronization
# ===========================

systemctl enable systemd-timesyncd
systemctl start systemd-timesyncd


# AUR
# ===

groupexists(){ getent group "$1" >/dev/null; }

aurinstall(){ echo "WARNING: yay has not been installed. Skipping installation of $@"; }

disable_sudo_password_for_pacman() { echo '%aur ALL=(ALL) NOPASSWD: /usr/bin/pacman' >/etc/sudoers.d/tmp; }
enable_sudo_password_for_pacman() { rm -rf /etc/sudoers.d/tmp; }

if [ "$SUDO" = 'y' ] && ! groupexists sudo; then
	pacinst sudo
	echo -e '# Allow members of group sudo to run any program\n%sudo ALL = (ALL) ALL' >/etc/sudoers.d/10_sudo
	groupadd sudo
fi

if [ "$YAY" = 'y' -a "$AUR" != 'y' ]; then
	echo 'setting AUR=y because that is required to install yay'
	AUR=y
fi

if [ "$AUR" = 'y' ] && ! groupexists aur; then
	pacinst git base-devel
	groupadd aur

	mkdir /aur
	chown root:aur /aur/
	chmod 775 /aur/

	echo -e '# Allow members of group aur to run pacman\n%aur ALL = (ALL) /usr/bin/pacman' >/etc/sudoers.d/20_aur
fi


# User
# ====

createuser() {
	echo "createuser $@"
	local username
	username="$1"
	useradd --create-home "$username"
	if [ "$AUR" = 'y' ]; then
		gpasswd -a "$username" aur
	fi
	if [ "$SUDO" = 'y' ]; then
		gpasswd -a "$username" sudo
	fi
	echo "Please enter a password for $username."
	while ! passwd "$username"; do
		echo "Please try again"
	done
	echo "done createuser"
}
if [ "${USERNAME-}" = "" ]; then
	read -p "Please enter a user name: " USERNAME
fi
if cat /etc/passwd | grep "^$USERNAME:" -q; then
	echo "user $USERNAME exists already, skipping user creation"
else
	createuser "$USERNAME"
fi

chown $USERNAME:$USERNAME /mnt/data/


# Auto Login
# ==========

if [ "$AUTOLOGIN" = "y" ]; then
	mkdir -p /etc/systemd/system/getty@tty1.service.d
	cat >/etc/systemd/system/getty@tty1.service.d/override.conf <<EOF
[Service]
ExecStart=
ExecStart=/usr/bin/agetty --autologin $USERNAME --noclear %I \$TERM
# do not wait until all active jobs are dispatched
Type=simple
# do not auto login again if you log out or the GUI crashes
# switch to another tty or reboot
Restart=no
EOF
fi


# yay
# ===

installyay() {
	local script="$(mktemp)"
	cat >"$script" <<'EOF'
	set -euo pipefail
	cd /aur
	git clone https://aur.archlinux.org/yay-bin.git
	cd yay-bin
	makepkg -si
EOF
	chmod +r "$script"
	disable_sudo_password_for_pacman
	echo "installing yay with $script"
	su - "$USERNAME" -c "bash '$script'"
	enable_sudo_password_for_pacman
}

if [ "$YAY" = 'y' ]; then
	if ! which yay &>/dev/null; then
		installyay
	fi
	aurinstall() {
		disable_sudo_password_for_pacman
		echo yay -S --noconfirm "$@"
		sudo --user="$USERNAME" yay -S --noconfirm "$@"
		enable_sudo_password_for_pacman
	}
fi


# System
# ======

pacman -Syu --noconfirm

pacinst dosfstools  # mkfs.fat
pacinst bash-completion
pacinst cryptsetup udisks2
pacinst ntfs-3g
pacinst reflector
pacinst pkgfile
pkgfile -u

pacinst python-pipx
pipxinstall() { su -c "pipx list | grep '$1' >/dev/null || pipx install '$1'" "$USERNAME"; }

pipxinstall git-viewer


# Window Server and Window Manager
# ================================

pacinst ttf-dejavu

if [ "$GNOME" = "y" ]; then
	pacinst gnome
	pacinst libreoffice-fresh

	script="$(mktemp)"
	cat >"$script" <<'EOF'
	echo 'XDG_SESSION_TYPE=wayland dbus-run-session gnome-session' >~/start-gnome
	chmod 555 ~/start-gnome
EOF
	chmod +r "$script"
	su - "$USERNAME" -c "bash '$script'"
fi

if [ "$SWAY" = "y" ]; then
	pacinst sway
	pacinst swaybg
	pacinst swaylock
	pacinst i3status
	pacinst bemenu bemenu-wayland
	pacinst wl-clipboard
	pacinst qt5-wayland
	pacinst grim
	pacinst wev

	if [ "$X11" = 'y' ]; then
		pacinst xorg-xwayland
		pacinst xorg-xeyes
	fi

	# keyboard layout is handled by sway config

	# start sway on login on tty1 if it's not running already
	bashprofile="/home/$USERNAME/.bash_profile"
	cat >"$bashprofile" <<'EOF'
export MOZ_ENABLE_WAYLAND=1
EOF
	if [ "$JAPANESE_INPUT" = "y" ]; then
		cat >>"$bashprofile" <<'EOF'
export QT_IM_MODULE='fcitx'
export SDL_IM_MODULE='fcitx'
export XMODIFIERS='@im=fcitx'
EOF
	fi
	cat >>"$bashprofile" <<'EOF'
[ -f ~/.profile ] && . ~/.profile
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" = 1 ]; then
	exec sway
else
	[ -f ~/.bashrc ] && . ~/.bashrc
fi
EOF
	chown "$USERNAME:$USERNAME" "$bashprofile"

elif [ "$X11" = "y" ]; then
	pacinst xorg-server xorg-apps xorg-xinit
	pacinst xclip
	pacinst xsel
	pacinst i3 dmenu
	pacinst scrot  # for screenshots

	# start i3 when X starts
	xinitrc="/home/$USERNAME/.xinitrc"
	sed -E '/twm|xterm|xclock/d' "/etc/X11/xinit/xinitrc" >"$xinitrc"
	echo >>"$xinitrc"
	echo "xset b off" >>"$xinitrc"
	if [ "${XKBMAP-}" != "" ]; then
		echo "setxkbmap $XKBMAP" >>"$xinitrc"
	fi
	echo 'exec i3' >>"$xinitrc"

	# start X on login on tty1 if it's not running already
	bashprofile="/home/$USERNAME/.bash_profile"
	cat >"$bashprofile" <<'EOF'
[ -f ~/.profile ] && . ~/.profile
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" = 1 ]; then
	exec startx
else
	[ -f ~/.bashrc ] && . ~/.bashrc
fi
EOF
	chown "$USERNAME:$USERNAME" "$bashprofile"

	checkvideodriver(){
		local graphicscard
		graphicscard="$(lspci -v | grep -A1 -e VGA -e 3D)"
		if echo "$graphicscard" | grep -i "Intel"; then
			echo "Installing mesa (an open-source implementation of the OpenGL specification)"
			pacinst mesa
			echo '"Some (Debian & Ubuntu, Fedora, KDE) recommend not installing the xf86-video-intel driver, and instead falling back on the modesetting driver for Gen4 and newer GPUs (GMA 3000 from 2006 and newer). However, the modesetting driver can cause problems such as Chromium Issue 370022 and vsync jitter/video stutter in mpv."'
			echo 'https://wiki.archlinux.org/title/intel_graphics'
			if ask_yes_no "Do you want to install the xf86-video-intel driver?"; then
				pacinst xf86-video-intel
			fi
		else
			echo "I don't know which driver to install for this graphics card:"
			echo "$graphicscard"
			echo "Please do so yourself."
			echo "https://wiki.archlinux.org/title/xorg#Driver_installation"
			bash
		fi
	}
	checkvideodriver
fi

profile="/home/$USERNAME/.profile"
echo '[ -d "$HOME/.local/bin" ] && export PATH="$PATH:$HOME/.local/bin"' >>"$profile"
echo '[ -d "$HOME/.cargo/bin" ] && export PATH="$PATH:$HOME/.cargo/bin"' >>"$profile"

chown "$USERNAME:$USERNAME" "$profile"


# IME
# ===

if [ "$JAPANESE_INPUT" = "y" ]; then
	# "Any particular reason for using fcitx rather than ibus?"
	# "Fcitx is gnome/dconf free and the interface skin can be customized, but functionality wise both should be pretty equivalent."
	# "Thanks! I just gave it a go (I used ibus until now), and it works well :)"
	#https://www.reddit.com/r/archlinux/comments/3tdd0c/comment/cxci33d/
	#https://confluence.jaytaala.com/display/TKB/Japanese+input+with+i3+and+Arch+based+distros
	#https://wiki.archlinux.org/title/Fcitx5
	pacinst fcitx5-im fcitx5-mozc
	# you still need to add `exec --no-startup-id fcitx5 -d` to your i3 config
	# and add MOZC as input method with `fcitx5-configtool`
	# but that is part of dot file management, not this installation script
fi


# Terminal
# ========

pacinst otf-ipafont
pacinst alacritty
pacinst kitty
# pillow is required for ranger `set preview_images_method kitty` (w3m does not work on wayland)
pacinst python-pillow


# Internet
# ========

pacinst qutebrowser
pacinst torbrowser-launcher firefox
pacinst torsocks

#TODO: firewall
#TODO: TOR


# Media
# =====

pacinst pulseaudio
pacinst pulsemixer

pacinst vlc
pacinst zvbi   # record webcam with vlc
pacinst exiv2  # image metadata
if [ "$SWAY" = y ]; then
	pacinst imv
else
	pacinst feh
fi

pacinst zathura
pacinst zathura-pdf-mupdf  # faster
#pacinst zathura-pdf-poppler  # more features, but which?
su "$USERNAME" -c 'xdg-mime default org.pwmt.zathura.desktop application/pdf'

pipxinstall yt-dlp


# CLI
# ===

pacinst ranger
pacinst fd
pacinst pacman-contrib
pacinst ncdu
pacinst bottom   # btm, e to expand, f to freeze, F9 to kill
pacinst zip unzip
# atool has moved to AUR

pacinst sdcv
pacinst w3m

if [ "$LAPTOP" = "y" ]; then
	pacinst brightnessctl
	pipxinstall crandr
fi


# User Software
# =============

if [ "$USER_SOFTWARE" = y ]; then
	# works on wayland
	pacinst quodlibet gst-plugins-good gst-libav
	pacinst playerctl
	# if quodlibet crashes with "ValueError: Namespace Soup not available" try `pacman -S libsoup`
	pacinst performous
	pacinst telegram-desktop
	pipxinstall castero  # podcast downloader
	pipxinstall yewtube

	pacinst noto-fonts
	pipxinstall udsync

	pacinst pdfarranger
	pacinst simple-scan

	# not yet running on wayland
	pacinst gimp
	pacinst gimp-plugin-gmic
	pacinst openshot

	# signal-desktop works on wayland with --enable-features=UseOzonePlatform --ozone-platform=wayland
	# now I just need to make it work without Android/iOS
fi

if [ "$DEVELOPMENT_TOOLS" = y ]; then
	pipxinstall ptpython

	pipxinstall mypy
	pipxinstall tox

	# install devpi to use pip and tox without internet
	#https://gitlab.com/erzo/cancli#running-the-tests
	pipxinstall devpi-server
	tmp="$(su "$USERNAME" -c 'mktemp -d')"
	su "$USERNAME" -c '~/.local/bin/devpi-init'
	(cd "$tmp"; su "$USERNAME" -c '~/.local/bin/devpi-gen-config')
	cp "$tmp/gen-config/devpi.service" /etc/systemd/system/
	systemctl daemon-reload
	systemctl start devpi.service
	systemctl enable devpi.service
	rm -r "$tmp"


	# required for vim tagbar
	# https://github.com/preservim/tagbar
	pacinst ctags

	pacinst sqlitebrowser
fi

if [ "$RUST" = y ]; then
	pacinst rustup
	su -c 'rustup default stable' "$USERNAME"
fi

if [ "$NEOVIM" = y ]; then
	pacinst neovim
	pacinst ripgrep  # required for telescope
fi

if [ "$NEOMUTT" = y ]; then
	pacinst neomutt
	pacinst isync
	pacinst msmtp
	pacinst w3m  # to view html e-mails
	pacinst libnotify
	pipxinstall mutt_ics
	if [ "$SWAY" = y ]; then
		#https://wiki.archlinux.org/title/Desktop_notifications#Notification_servers
		pacinst mako
	fi
fi

if [ "$LATEX" = y ]; then
	pacinst texlive biber
	pacinst python-pygments  # required for code highlighting with minted
	pacinst texlive-langgerman
	pacinst texlive-binextra  # contains the texdoc binary
	pacinst texlive-doc   # contains the pdfs opened by texdoc
	if [ "$JAPANESE_INPUT" = y ]; then
		pacinst texlive-langjapanese
		pacinst ttf-hanazono
		pacinst adobe-source-han-sans-jp-fonts
		pacinst texlive-langchinese  # https://bugs.archlinux.org/task/67856
	fi
fi


# Dot File Management
# ===================

init_dotfiles() {
	echo "init_dotfiles $@"
	local username
	username="$1"

	if su -c '[ -d ~/.dotfiles ]' "$username"; then
		echo "~/.dotfiles exists already. I am skipping initializing the dotfile repositories."
		return
	fi

	#https://wiki.archlinux.org/title/Dotfiles#Tracking_dotfiles_directly_with_Git
	su -c 'git init --bare ~/.dotfiles' "$username"
	su -c 'git --git-dir="$HOME/.dotfiles/" --work-tree="$HOME" config status.showUntrackedFiles no' "$username"

	su -c 'git init --bare ~/.dotfiles-private' "$username"
	su -c 'git --git-dir="$HOME/.dotfiles-private/" --work-tree="$HOME" config status.showUntrackedFiles no' "$username"
	su -c 'chmod 700 ~/.dotfiles-private' "$username"

	su "$username" -c 'cat >>~/.bashrc' <<'EOF'

_git_use_bare_repo()
{
	local GIT_WORK_TREE="$1"
	local GIT_DIR="$2"
	export GIT_WORK_TREE GIT_DIR
	shift 2
	if [ $# = 0 ]; then
		bash --rcfile <(cat ~/.bashrc; echo "export GIT_DIR='$GIT_DIR' GIT_WORK_TREE='$GIT_WORK_TREE'; PS1=\"[GIT_DIR=\\\$GIT_DIR]\$PS1\"; echo 'GIT_DIR=$GIT_DIR'; git status")
	elif [ "$1" = "gitl" -o "$1" = "gitd" -o "$1" = "gits" ]; then
		"$@"
	else
		git "$@"
	fi
}
alias config='_git_use_bare_repo ~ ~/.dotfiles'
alias pconfig='_git_use_bare_repo ~ ~/.dotfiles-private'
source /usr/share/bash-completion/completions/git
__git_complete config __git_main
__git_complete pconfig __git_main
EOF
	# If you have trouble with fcitx5 overwriting your config file try
	# git checkout ~/.config/fcitx5/profile; fcitx5-remote -r
	echo "done init_dotfiles"
}

cp_dotfiles() {
	echo "cp_dotfiles $@"
	local IFS username fn_script
	username="$1"
	
	if ! [ -d '/etc/install' ]; then
		echo >&2 '/etc/install does not exist. I am assuming this is a 2nd run and the dotfiles have been copied already.'
		return
	fi
	
	chown -R "$username:$username" /etc/install
	
	IFS=:
	fn_script="$(mktemp)"
	for name in $DOTFILES; do
		cat >"$fn_script" <<EOF
set -euo pipefail
dst=~/'.${name#dot_}'
if [ -d "\$dst" ]; then
	echo  "\$dst exists already"
	echo  "I am skipping it"
	exit 0
else
	cp -r '/etc/install/dotfiles/$name' "\$dst"
fi

cd
# submodules are specified in the work tree in .gitmodules
# therefore only one dotfiles repository can contain submodules
# I am running submodule update only for the repo which creates the .gitmodules file
submodules_before="\$(ls -1 ~/.gitmodules 2>/dev/null || true)"
echo git --work-tree ~ --git-dir ~/'.${name#dot_}' checkout ~
git --work-tree ~ --git-dir ~/'.${name#dot_}' checkout ~
submodules_after="\$(ls -1 ~/.gitmodules 2>/dev/null || true)"
if [ "\$submodules_before" != "\$submodules_after" ]; then
	echo git --work-tree ~ --git-dir ~/'.${name#dot_}' submodule update --init
	git --work-tree ~ --git-dir ~/'.${name#dot_}' submodule update --init
fi
EOF
		chmod o+r "$fn_script"
		su -c "bash '$fn_script'" "$username"
	done
	rm "$fn_script"
	echo "done cp_dotfiles"
}

pacinst git
if [ "$DOTFILES" = "" ]; then
	:
elif [ "$DOTFILES" = "new" ]; then
	init_dotfiles "$USERNAME"
else
	cp_dotfiles "$USERNAME"
fi


# passwords
# =========

init_pass() {
	echo "init_pass $@"
	local username
	local fn_script
	username="$1"
	fn_script="$(mktemp)"

	chown -R "$username:$username" /etc/install

	cat >"$fn_script" <<END_OF_SCRIPT
	set -euo pipefail
	IFS="\$(printf '\n\t')"

	key_fpr_to_be_deleted=
	get_key_fingerprints() { gpg --list-keys --fingerprint --with-colons | sed -n '/^fpr/p' | cut -d: -f10; }
	if [ "$COPY_PASSWORDS" = "y" ]; then
		rm -r ~/.password-store  2>/dev/null || true
		rm -r ~/.gnupg  2>/dev/null || true
		cp -r "/etc/install/dot_password-store" ~/.password-store
		cp -r "/etc/install/dot_gnupg" ~/.gnupg
		chmod 700 ~/.password-store
		chmod 700 ~/.gnupg
		# remove gpg-agent.conf so that reinitializing pass requires only one password entry
		rm ~/.gnupg/gpg-agent.conf

		key_fpr_to_be_deleted="\$(get_key_fingerprints)"
		existing_keys="\$(gpg --list-keys | sed -En 's/^uid +\[[^]]+\] +(.*)/\1/p')"
		is_existing_key () { echo "\$existing_keys" | grep -Fxq "\$1"; }
		if is_existing_key "$NEW_PASS_KEY"; then
			tmp_key_name=temporary-key
			while is_existing_key "\$tmp_key_name"; do
				tmp_key_name="\$tmp_key_name-2"
			done
		else
			tmp_key_name=
		fi
	else
		mkdir ~/.gnupg
		chmod go-rwx ~/.gnupg
		tmp_key_name=
	fi

	if [ "$NEW_PASS_KEY" != "" ]; then
		# create tmp key if new key has the same name like old key
		if [ "\$tmp_key_name" != "" ]; then
			gpg --batch --pinentry-mode loopback --passphrase tmp --quick-gen-key "\$tmp_key_name" default default seconds=3600

			# decrypt an arbitrary password manually to unlock the key because I cannot pass --pinentry-mode to pass
			fn="$(find /etc/install/dot_password-store -type f | head -1)"
			echo "Please enter the old password to decrypt the password store"
			while ! gpg -d --pinentry-mode loopback "\$fn" >/dev/null; do
				read -p "Failed to decrypt key. Do you want to try again? [Y/n] " -r ans
				if [ "\$ans" = "n" ]; then
					echo "Skipping reinitialization of pass"
					exit 0
				fi
			done

			pass init "\$tmp_key_name"

			# use tmp_key_name so that the passphrase is cached and the next pass init won't ask for it
			fn="\$(mktemp)"
			gpg -r "\$tmp_key_name" -e "\$fn"
			gpg --batch --pinentry-mode loopback --passphrase tmp -d "\$fn.gpg" 2>/dev/null
		fi

		# remove old keys so that I can create the new key
		if [ "\$key_fpr_to_be_deleted" != "" ]; then
			while IFS= read -r fpr; do
				gpg --delete-secret-and-public-keys --batch --yes "\$fpr" && echo "deleted key \$fpr"  || true
			done <<<"\$key_fpr_to_be_deleted"
			key_fpr_to_be_deleted="\$(get_key_fingerprints)"
		fi

		# create new key
		echo "Please enter a new password to encrypt the password store"
		gpg --pinentry-mode loopback --quick-gen-key "$NEW_PASS_KEY" default default never
		pass init "$NEW_PASS_KEY"

		# remove tmp key
		if [ "\$key_fpr_to_be_deleted" != "" ]; then
			while IFS= read -r fpr; do
				gpg --delete-secret-and-public-keys --batch --yes "\$fpr" && echo "deleted key \$fpr"  || true
			done <<<"\$key_fpr_to_be_deleted"
			echo "You don't need to worry about keys which have not been deleted because they don't exist."
			echo "They are sub keys which have already been deleted when their corresponding main key was deleted."
		fi
	fi

	cat >~/.gnupg/gpg-agent.conf <<'EOF'
default-cache-ttl 0
max-cache-ttl 0
EOF
	gpgconf --kill gpg-agent
END_OF_SCRIPT

	chmod o+r "$fn_script"
	echo sudo -u "$username" bash "$fn_script"
	sudo -u "$username" bash "$fn_script"
	echo "done init_pass"
}

if [ "$PASS" = "y" ]; then
	pacinst pass
	pacinst sudo
	pipxinstall pass-export
	init_pass "$USERNAME"
	if [ "$SUDO" != "y" -a "$AUR" != 'y' ]; then
		pacman -Rs --noconfirm sudo
	fi
fi
if [ -d "/etc/install" ]; then
	# -f is required because it contains  git repositories
	rm -rf "/etc/install"
fi


# Work
# ====

if [ "$WORK" = "y" ]; then
	pacinst freecad
	pacinst openvpn
	pacinst sshpass

	aurinstall can-utils
	aurinstall vscodium-bin
	aurinstall teams
fi


# End
# ===

echo
echo "All done."
poweroff
