#!/usr/bin/env bash

ask_yes_no() {
	local ans
	while true; do
		read -p "$1 [Y/n] " -r ans
		if [ "$ans" = "y" -o "$ans" = "Y" ]; then
			return 0
		elif [ "$ans" = "n" -o "$ans" = "N" ]; then
			return 1
		elif [ "$ans" = "" ]; then
			return 0
		else
			echo "Invalid input '$ans'."
		fi
	done
}

menu() {
	local tmp bak num_values
	tmp="`mktemp`"
	echo >>"$tmp" "# $@"
	echo >>"$tmp" "# Please uncomment the value you want to use, write and quit."
	echo >>"$tmp" ""
	sed 's/^/# /' >>"$tmp"
	bak="`mktemp`"
	cp "$tmp" "$bak"
	while true; do
		vim "$tmp" </dev/tty >/dev/tty
		num_values="`grep -c '^[^#]' "$tmp"`"
		if [ "$num_values" = 1 ]; then
			break
		elif [ "$num_values" -gt 1 ]; then
			echo "Too many lines uncommented ($num_values instead of 1)." >/dev/tty
		else
			echo "No line uncommented." >/dev/tty
		fi
		if ask_yes_no "Do you want to reset your changes?" >/dev/tty </dev/tty; then
			cp "$bak" "$tmp"
		fi
	done
	sed -En 's/^ *([^#]+)/\1/p' "$tmp"
	rm "$tmp"
	rm "$bak"
}


has_internet() {
	ping -c 1 archlinux.org >/dev/null
}
