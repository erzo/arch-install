An installation script to install [Arch Linux](https://wiki.archlinux.org/title/Arch_Linux) on a computer or USB stick.

1. Clone this repository

   ```bash
   git clone https://gitlab.com/erzo/arch-install
   cd arch-install
   ```

2. Connect a USB stick with no important data on it.
   It will be turned into a [live USB](https://en.wikipedia.org/wiki/Live_USB) and all data on it will be lost.

3. Run

   ```bash
   ./0-preparation.sh
   ```

   It will download an Arch Linux image and write it to the USB stick.
   It will also try to gather the following settings from the current operating system and write them to config files for the two upcoming scripts:

   - LOCALE (the language)
   - KEYMAP (the keyboard layout used in a [console](https://wiki.archlinux.org/title/Linux_console), i.e. without graphical user interface)
   - XKBMAP (the keyboard layout used when a graphical user interface is running)
   - TIMEZONE

   If you explicitly opt in it will also copy your [dotfiles](https://wiki.archlinux.org/title/Dotfiles) and [gpg keys](https://wiki.archlinux.org/title/GnuPG).
   Dotfiles are assumed to be stored in bare git repo(s) in `~/.dotfiles` and/or `~/.dotfiles-private` as explained in the [Arch Wiki](https://wiki.archlinux.org/title/Dotfiles#Tracking_dotfiles_directly_with_Git).
   If you have named the repo(s) differently you can specify them with the `--dotfiles` option.

   Settings, dotfiles, gpg keys and the following scripts are written to a third partition on the live stick.

   For more information run

   ```bash
   ./0-preparation.sh --help
   ```

4. Connect the USB stick to the computer where you want to install Arch Linux and turn it on.
   Depending on the BIOS settings it will boot from the live stick automatically or you need to press some key to select the boot device.

5. Mount the third partition of the live stick.
   I am making a bitwise copy of it first so that I am not changing the installation stick.

   If needed you can change the keyboard layout with the following command.
   This is only temporarily for the installation.

   ```bash
   loadkeys de
   ```

   The following commands assume that the internal HDD/SSD is `/dev/sda` and that no other block devices are connected.
   If the internal HDD/SSD is not `/dev/sda` replace `/dev/sdb` with `/dev/sda`.

   ```bash
   dd if=/dev/sdb3 of=install.img
   mkdir install
   mount install.img install
   cd install
   ```

   If you want to install Arch Linux on a USB stick instead of on the internal HDD/SSD plug in the USB stick now.

   ```bash
   ./1-setup.sh
   ```

   If there are several block devices the script will ask you on which device you want to install Arch Linux.

6. The config file for this script will be opened in [vim](https://www.vim.org/).

   - Press `i` to change to insert mode.
   - Enter a value for `HOSTNAME`.
   - Check that the other settings are to your liking.

     - `KEYMAP`: The keyboard layout used in a [console](https://wiki.archlinux.org/title/Linux_console), i.e. without graphical user interface
     - `TIMEZONE`: A path relative to `/usr/share/zoneinfo/`
     - `LOCALE`: The value of the [LANG variable](https://wiki.archlinux.org/title/Locale#LANG:_default_locale)
     - `HOSTNAME`: The [hostname](https://wiki.archlinux.org/title/Hostname#Set_the_hostname)
     - `LOCALDOMAIN`: TODO: I think this was intended for [/etc/hosts](https://wiki.archlinux.org/title/Hostname#localhost_is_resolved_over_the_network) but this is not written to any file and everything works without it => remove this

     - `ENCRYPT`: `y` for yes or `n` for no. If yes: All partitions except for the boot partition are logical volumes on one big encrypted partition, see [LVM on LUKS](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS). If no: All partitions are primary partitions.
     - `WIPE`: `y` for yes or `n` for no. Overwrite the entire device with random numbers before formating it. This is [recommended](https://wiki.archlinux.org/title/Dm-crypt/Drive_preparation) the first time you create an encrypted partition.
     - `CRYPTSETUP`: [options for cryptsetup](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Encryption_options_for_LUKS_mode)
     - `BENCHMARK`: `y` for yes or `n` for no. If yes: Run a benchmark to see which cipher is the fastest on this system.
     - `ON_USB`: `y` for yes or `n` for no. If yes: Reduce number of writes to increase the lifetime of the device (see [Arch Wiki](https://wiki.archlinux.org/title/Install_Arch_Linux_on_a_removable_medium#Minimizing_disk_access) and [Mags Zone](https://mags.zone/help/arch-usb.html#noatime)) and remove autodetect hook [to allow booting on multiple systems each requiring different modules](https://wiki.archlinux.org/title/Install_Arch_Linux_on_a_removable_medium#Installation_tweaks).

     - `DEV`: The target device
     - `PART_SIZE_BOOT`: The size of the `/boot` partition. Supported units: `MiB`, `GiB` and `%` of the size of the target device.
     - `PART_SIZE_ROOT`: The size of the `/` partition
     - `PART_SIZE_VAR`: The size of the `/var` partition
     - `PART_SIZE_SWAP`: The size of the swap partition, `auto` means two times the RAM if there is less than 1GB of RAM, otherwise the same as RAM
     - `PART_SIZE_FREE`: How much space should be left free. This inderectly defines the size of the `/data` partition.

     - `UDEV` one of `auto`, `y` or `n`: If the system is encrypted this determines the [mkinitcpio hooks](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Configuring_mkinitcpio_3) and [kernel parameters](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Configuring_the_boot_loader_2).

   - Press escape to change to normal mode.
   - Type `:wq` and confirm with enter to save and close the file.

   The config file will be [sourced](https://ss64.com/bash/source.html) by the shell script.

   The script will format the selected device and install Arch Linux.
   The next installation script and it's config file are copied to `/root`.
   The passwords and keys are copied to `/etc/install`.
   When the installation is successful the computer powers off.

7. Disconnect the live stick and boot into your new operating system.

8. Run

   ```bash
   ./2-post-install.sh
   ```

9. The config file for this script will be opened in [vim](https://www.vim.org/).

   - Press `i` to change to insert mode.
   - Enter a value for `USERNAME`.
   - You can disable copying dotfiles by removing the directory from `DOTFILES`.
   - You can disable copying passwords by setting `COPY_PASSWORDS` to `n`.
   - Select which programs should be installed by setting the groups to either `y` for yes or `n` for no.
     Check the code of [2-post-install.sh](./2-post-install.sh) to see which programs are included in which groups.

      - `SWAY`: Install the wayland compositor [sway](https://swaywm.org/)
      - `X11`: If `SWAY` is `y` install [Xwayland](https://wiki.archlinux.org/title/Wayland#Xwayland), if `SWAY` is `n` install the [i3](https://i3wm.org/) window manager
      - `GNOME`: Install the desktop environment [Gnome](https://www.gnome.org/)
      - `JAPANESE_INPUT`: Install [Mozc](https://wiki.archlinux.org/title/Mozc) on [fcitx5](https://wiki.archlinux.org/title/Fcitx5)
      - `LAPTOP`: Install software specific for laptops
      - `USER_SOFTWARE`
      - `DEVELOPMENT_TOOLS`
      - `LATEX`: Install [LaTeX](https://www.latex-project.org/), a software to generate high quality pdfs
      - `NEOMUTT`: Install [neomutt](https://neomutt.org/), an e-mail client
      - `PASS`: Install [pass](https://www.passwordstore.org/), a password manager
      - `AUR`: Prepare the system to install packages from the [Arch User Repository](https://wiki.archlinux.org/title/Arch_User_Repository)
      - `YAY`: Install [yay](https://github.com/Jguer/yay), a wrapper around [pacman](https://wiki.archlinux.org/title/Pacman) to install software from the [Arch User Repository](https://wiki.archlinux.org/title/Arch_User_Repository)
      - `SUDO`: Install [sudo](https://wiki.archlinux.org/title/Sudo), a tool to run software as another user
      - `WORK`: Install software which I need for work or am supposed to have installed

   - Other settings:

      - `USERNAME`: The name of the user to be created
      - `COPY_PASSWORDS`: `y` for yes or `n` for no. If yes: copy the passwords from `/etc/install`
      - `XKBMAP`: Arguments passed to [setxkbmap](https://wiki.archlinux.org/title/Xorg/Keyboard_configuration#Setting_keyboard_layout) specifying the keyboard layout used when a graphical user interface is running
      - `AUTOLOGIN`: `y` for yes or `n` for no. If yes: Log in the user automatically on tty1 without the need to insert a password. This is useful if you encrypt the HDD/SSD so that you don't need to insert two passwords when booting the computer.
      - `DOTFILES`: A colon separated list of directories containing bare git repositories. The are copied from `/etc/install/dotfiles` to the home directory and checked out.
      - `NEW_PASS_KEY`: If `PASS` is `y`: The name of the key used for the password manager pass. If `NEW_PASS_KEY` is not empty a new key is created. If `NEW_PASS_KEY` is empty and `COPY_PASSWORDS` is `y` the old key is reused.

   - Press escape to change to normal mode.
   - Type `:wq` and confirm with enter to save and close the file.

   The config file will be [sourced](https://ss64.com/bash/source.html) by the shell script.

   The script will create a new user, copy the dotfiles specified in `DOTFILES`, copy the passwords if `COPY_PASSWORDS` is `y` and install the programs selected with `y`.
   When the installation is successful `/etc/install` is removed and the computer is powered off.
