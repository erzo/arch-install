#!/usr/bin/env sh
#https://wiki.archlinux.org/title/Installation_guide

# Enable Strict Mode
# ==================
#http://redsymbol.net/articles/unofficial-bash-strict-mode/

get_shell() {
	local shell
	shell="`readlink /proc/$$/exe`"
	echo "${shell##*/}"
}

IFS="$(printf '\n\t')"
set -eu
if [ "`get_shell`" = "bash" ]; then
	set -o pipefail
fi


# Options
# =======

ECHO=y
EXEC=y

run() {
	# $ run echo "hello world"
	# works for commands with arguments
	# does not work for pipes, redirects and stuff
	if [ "$ECHO" = "y" ]; then
		echo "$@"
	fi
	if [ "$EXEC" = "y" ]; then
		"$@"
	fi
}
runstr() {
	# $ runstr 'echo "hello world" >file'
	# pass command as string
	if [ "$ECHO" = "y" ]; then
		echo "$@"
	fi
	if [ "$EXEC" = "y" ]; then
		eval "$@"
	fi
}


# Boot mode
# =========

#https://bbs.archlinux.org/viewtopic.php?id=197337
if ! ls /sys/firmware/efi >/dev/null 2>&1; then
	echo "This installer supports UEFI installs only."
	echo "Please reboot into UEFI."
	exit 1
fi


# Settings
# ========

. ./ui.sh

available_keymaps() {
	find /usr/share/kbd/keymaps/ -type f -name "*.map.gz" | grep -o '[^/]*$' | grep -o '^[^.]*' | sort | uniq
}
available_timezones() {
	find /usr/share/zoneinfo -mindepth 2 -maxdepth 2 -type f | sort
}
available_locales() {
	sed -En 's/^(# *)?([^ ]+) +([^ ]+)$/\2/p' /etc/locale.gen
}

get_ram_mebi() {
	vmstat --unit M -s | sed -En 's/^ *([0-9]+) M total memory$/\1/p'
}
calc_swap() {
	local ram
	ram="`get_ram_mebi`"
	if [ "$ram" -le 1000 ]; then
		echo "$((2*$ram))MiB"
	else
		echo "${ram}MiB"
	fi
}

DEV="$(python get_unused_device.py)"
if [ -f settings ]; then
	if grep -q 'DEV=AUTO' settings; then
		sed -i.bak "s:DEV=AUTO:DEV=$DEV:" settings
	fi
	vim settings
	. ./settings
elif ! ask_yes_no "Failed to find settings. Do you want to proceed? "; then
	exit
fi

if [ "${KEYMAP:=}" = "" ]; then
	if ask_yes_no "No keymap specified. Do you want to select one?"; then
		KEYMAP="`available_keymaps | menu "Please select a keymap."`"
	else
		echo "Not setting a keymap."
	fi
fi
if [ "${TIMEZONE-}" = "" ]; then
	TIMEZONE="`available_timezones | menu "Please select a time zone."`"
fi
if [ "${LOCALE-}" = "" ]; then
	LOCALE="`available_locales | menu "Please select a locale."`"
fi

if [ "${HOSTNAME-}" = "" ]; then
	read -p "Please insert a host name: " -r HOSTNAME
fi
LOCALDOMAIN="${LOCALDOMAIN-mydomain}"

# I am not encrypting the /boot partition because GRUB does not support LUKS2 yet
# https://wiki.archlinux.org/title/GRUB#LUKS2
# and "You should definitely use LUKS2 whenever possible. It is the newer header format and overcomes the limits of the (legacy) LUKS1 header."
# https://askubuntu.com/questions/1032546/should-i-use-luks1-or-luks2-for-partition-encryption/1259424#1259424
# Also "LUKS2 [provides] redundant information to provide recovery in the case of corruption"
# https://askubuntu.com/questions/1032546/should-i-use-luks1-or-luks2-for-partition-encryption/1062729#1062729
# Encrpyting /boot would require a few more steps than expected: a keyfile would need to be added to initramfs (that's no problem, though)
# https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Encrypted_boot_partition_(GRUB)
ENCRYPT="${ENCRYPT-y}"
WIPE="${WIPE-$ENCRYPT}"
BENCHMARK=${CRYPTSETUP+n}
BENCHMARK=${BENCHMARK:-y}
CRYPTSETUP="${CRYPTSETUP-}"

# ON_USB caused crashes
ON_USB="${ON_USB-n}"

PART_SIZE_BOOT="${PART_SIZE_BOOT-300MiB}"
PART_SIZE_ROOT="${PART_SIZE_ROOT-20GiB}"
PART_SIZE_VAR="${PART_SIZE_VAR-10GiB}"
PART_SIZE_SWAP="${PART_SIZE_SWAP-auto}"
PART_SIZE_FREE="${PART_SIZE_FREE-50%}"

if [ "$PART_SIZE_SWAP" = "auto" ]; then
	if [ "$ON_USB" = 'y' ]; then
		PART_SIZE_SWAP=
	else
		PART_SIZE_SWAP="`calc_swap`"
	fi
fi

NAME_DECRYPTED=cryptlvm
VOLUME_GROUP=vga

UDEV=auto


# Pre-installation
# ================

# Set the keyboard layout
[ "$KEYMAP" != "" ] && run loadkeys "$KEYMAP"

# Connect to the internet
if [ "$EXEC" = "y" ] && ! has_internet; then
	echo "no internet connection, please configure wifi"
	run rfkill unblock wlan
	run iwctl
	if ! ping -c 1 archlinux.org >/dev/null; then
		echo "still no internet connection"
		exit 1
	fi
fi

# Update the system clock
run timedatectl set-ntp true

# wipe
if [ "$WIPE" = "y" ]; then
	run dd if=/dev/urandom of=$DEV bs=4096 status=progress || true
fi

# Partition the disks
start=1MiB
partition_counter=0

if ! which blockdev >/dev/null; then
	# it seems blockdev is not available as user command but does not need root permissions
	export PATH="$PATH:/usr/sbin"
fi

dev_size_in_mebi() {
	local size_in_bytes
	size_in_bytes=`blockdev --getsize64 $DEV`
	echo $(( $size_in_bytes / 1024 / 1024 ))
}

size_to_mebi() {
	local value unit
	value="`echo "$1" | sed -E 's/^(-?[0-9]+) *([^ ]*)$/\1/'`"
	unit="`echo "$1" | sed -E 's/^(-?[0-9]+) *([^ ]*)$/\2/'`"
	if [ "$unit" = "MiB" ]; then
		: # no conversion needed
	elif [ "$unit" = "GiB" ]; then
		value=$(( $value * 1024 ))
	elif [ "$unit" = "%" ]; then
		local total
		total=`dev_size_in_mebi`
		value=$(( $value * $total / 100 ))
	else
		echo >&2 "unsupported unit '$unit'"
		return 1
	fi
	echo "${value}"
}

add_sizes() {
	local size1 size2
	size1="`size_to_mebi "$1"`"
	size2="`size_to_mebi "$2"`"
	echo "$(( $size1 + $size2 ))MiB"
}

get_first_char() {
	#https://stackoverflow.com/a/44988512
	echo "${1%"${1#?}"}"
}

mkpart() {
	local name fstype size
	name="$1"
	fstype="$2"
	size="$3"
	# if size is negative it's the size that is supposed to be left free
	if [ "$(get_first_char "$size")" = "-" ]; then
		local total
		total="`dev_size_in_mebi`MiB"
		end=$(add_sizes "$total" "$size")
	else
		end=$(add_sizes "$start" "$size")
	fi
	partition_counter=$(( $partition_counter + 1 ))
	if echo "$DEV" | grep -Eq ".+[0-9]$"; then
		# mmcblk1 -> mmcblk1p1
		CREATED_PART="${DEV}p$partition_counter"
	else
		# sda -> sda1
		CREATED_PART="$DEV$partition_counter"
	fi
	run parted "$DEV" mkpart "$name" "$fstype" "$start" "$end"
	if [ "$ECHO" = "y" ]; then
		echo "    size of $DEV$partition_counter ($name): $(( (`size_to_mebi $end` - `size_to_mebi $start`) / 1024 ))GiB"
	fi
	start="$end"
}

encrypt() {
	if [ "$BENCHMARK" = "y" ]; then
		local cipher
		echo "running benchmarks to decide on cypher"
		#TODO this works when copying it in a bash but not in the real script. cipher remains empty. Neither on Debian nor on Arch.
		# I multiply (instead of add) encryption and decryption speed in order to prefer ciphers with symmetric speed
		cipher=$(cryptsetup benchmark | tee >/dev/stderr | perl -nae 'next unless /^ /; print @F[2]*@F[4]; print "  @F[0] @F[1]\n"' | sort -nr | head -1 | cut -f3,4 -d' ')
		cryptsetup --help | sed -n '/Default compiled-in/,$p'
		echo "Fastest cipher on this system: $cipher"
		echo "Enter cryptsetup options as desired (e.g. --cipher=aes-xts-plain64 --key-size=512)"
		read -p ">>> " -r CRYPTSETUP
	fi

	# fs-type = -1 (https://unix.stackexchange.com/questions/103083/parted-create-new-partition)
	mkpart "main" "" "-0%"
	PART_ENCRYPTED="$CREATED_PART"

	head /dev/urandom --bytes 128 >key
	runstr "cat key | cryptsetup $CRYPTSETUP luksFormat '$PART_ENCRYPTED' -"
	run cryptsetup open "$PART_ENCRYPTED" "$NAME_DECRYPTED" --key-file key
	PART_DECRYPTED="/dev/mapper/$NAME_DECRYPTED"
	while ! run cryptsetup luksChangeKey "$PART_ENCRYPTED" --key-file key; do
		echo "Please try again"
	done
	rm key

	run pvcreate "$PART_DECRYPTED"
	run vgcreate "$VOLUME_GROUP" "$PART_DECRYPTED"

	mkpart() {
		local name fstype size
		name="$1"
		fstype="$2"
		size="$3"
		if [ "$(get_first_char "$size")" = "-" ]; then
			local total
			total="`dev_size_in_mebi`"
			size="$(size_to_mebi "$(add_sizes "$total MiB" "$size")")"
			size="-l$(( 100 * $size / $total ))%FREE"
		else
			#size_to_mebi to convert units
			size="-L`size_to_mebi "$size"`m"
		fi
		CREATED_PART="/dev/$VOLUME_GROUP/$name"
		run lvcreate "$size" "$VOLUME_GROUP" -n "$name"
	}
}

run parted $DEV -s mklabel gpt

mkpart "boot" "fat32" "$PART_SIZE_BOOT"
PART_BOOT="$CREATED_PART"
run parted "$DEV" set "$partition_counter" esp on

if [ "$ENCRYPT" = "y" ]; then
	encrypt
fi

mkpart "root" "ext4" "$PART_SIZE_ROOT"
PART_ROOT="$CREATED_PART"

if [ "$PART_SIZE_SWAP" != "" ]; then
	mkpart "swap" "linux-swap" "$PART_SIZE_SWAP"
	PART_SWAP="$CREATED_PART"
else
	PART_SWAP=
fi

if [ "$PART_SIZE_VAR" != "" ]; then
	mkpart "var"  "ext4" "$PART_SIZE_VAR"
	PART_VAR="$CREATED_PART"
else
	PART_VAR=
fi

mkpart "data" "ext4" "-$PART_SIZE_FREE"
PART_DATA="$CREATED_PART"

# Format the partitions
if [ "$ON_USB" = "y" ]; then
	ext4_options='-O ^has_journal'
else
	ext4_options=
fi
run mkfs.fat -F 32 -I $PART_BOOT
run mkfs.ext4 $ext4_options -F $PART_ROOT
if [ "$PART_VAR" != "" ]; then
	run mkfs.ext4 $ext4_options -F $PART_VAR
fi
run mkfs.ext4 $ext4_options -F $PART_DATA
if [ "$PART_SWAP" != "" ]; then
	run mkswap $PART_SWAP
fi

# Mount the file systems
run mount $PART_ROOT /mnt
run mkdir /mnt/boot
run mount $PART_BOOT /mnt/boot
if [ "$PART_VAR" != "" ]; then
	run mkdir /mnt/var
	run mount $PART_VAR /mnt/var
fi
run mkdir -p /mnt/mnt/data
run mount $PART_DATA /mnt/mnt/data
if [ "$PART_SWAP" != "" ]; then
	run swapon $PART_SWAP
fi


# Installation
# ============

# Select the mirrors
#run vim /etc/pacman.d/mirrorlist

# Install essential packages
pacman -Sy --noconfirm archlinux-keyring
run pacstrap /mnt base linux linux-firmware
run pacstrap /mnt man-db man-pages
run pacstrap /mnt vim
run pacstrap /mnt efibootmgr
run pacstrap /mnt grub
run pacstrap /mnt intel-ucode
run pacstrap /mnt amd-ucode


# Configure the system
# ====================

# Fstab
runstr 'genfstab -U /mnt >> /mnt/etc/fstab'
#run vim /mnt/etc/fstab

runaschroot() {
	local script

	script="/mnt/root/install"
	cat >"$script" <<EOF
	#!/usr/bin/env bash
	set -euo pipefail
	IFS="$(printf '\n\t')"

	echo 'Disable beeping'
	sed -Ei.bak 's/#\s*(set bell-style none)/\1/' /etc/inputrc
	echo 'blacklist pcspkr' >'/etc/modprobe.d/nobeep.conf'
	rmmod pcspkr

	echo 'Time zone'
	ln -sf "$TIMEZONE" /etc/localtime
	hwclock --systohc

	echo 'Localization'
	# comment out all lines
	sed -i.bak 's/^[^#]/#&/' /etc/locale.gen
	# uncomment desired locale
	LOCALE_RE="$(echo "$LOCALE" | sed 's/\./\\&/g' | sed 's/utf8/utf-?8/I')"
	sed -Ei "s/# *(\$LOCALE_RE)/\1/I" /etc/locale.gen
	# verify that exactly one line is uncommented
	NUMBER_LOCALS="\$(grep -c "^[^#]" /etc/locale.gen || true)"
	if [ "\$NUMBER_LOCALS" = 0 ]; then
		echo "locale $LOCALE not found in /etc/locale.gen"
		exit 1
	fi
	if [ "\$NUMBER_LOCALS" -gt 1 ]; then
		echo "! several uncommented lines /etc/locale.gen, please double check"
		vim /etc/locale.gen '+/^[^#]'
	fi
	# generate the locales
	locale-gen
	echo "LANG=$LOCALE" > /etc/locale.conf
	# make keyboard setting persistent
	[ "$KEYMAP" != "" ] && echo "KEYMAP=$KEYMAP" > /etc/vconsole.conf


	echo 'Network configuration'
	echo '$HOSTNAME' > /etc/hostname
	echo '127.0.1.1	$HOSTNAME.$LOCALDOMAIN	$HOSTNAME'

	pacman -Syu --noconfirm networkmanager


	echo 'Root password'
	while ! passwd; do
		echo "Please try again"
	done

	if [ "$ENCRYPT" = "y" ]; then
		echo 'Add decryption tools to initial RAM'
		pacman -S --noconfirm lvm2
		if [ "$UDEV" = "auto" ]; then
			if grep -q '^HOOKS=.* udev ' /etc/mkinitcpio.conf; then
				UDEV=y
			else
				UDEV=n
			fi
		else
			UDEV="$UDEV"
		fi
		if [ "\$UDEV" = "y" ]; then
			HOOKS='HOOKS=(base udev autodetect keyboard keymap consolefont modconf block encrypt lvm2 filesystems fsck)'
		else
			HOOKS='HOOKS=(base systemd autodetect keyboard sd-vconsole modconf block sd-encrypt lvm2 filesystems fsck)'
		fi
		if [ "$ON_USB" = "y" ]; then
			# if installation on USB stick remove autodetect hook
			# "to allow booting on multiple systems each requiring different modules in early userspace"
			# https://wiki.archlinux.org/title/Install_Arch_Linux_on_a_removable_medium#Installation_tweaks
			# https://mags.zone/help/arch-usb.html#mkinitcpio
			HOOKS="\${HOOKS/ autodetect / }"
		fi
		cp /etc/mkinitcpio.conf /etc/mkinitcpio.conf.bak
		sed -i "s/^HOOKS.*$/\$HOOKS/" /etc/mkinitcpio.conf
		mkinitcpio -p linux
	elif [ "$ON_USB" = "y" ]; then
		echo 'Remove autodetect hook to allow booting on multiple systems each requiring different modules in early userspace'
		cp /etc/mkinitcpio.conf /etc/mkinitcpio.conf.bak
		sed -i '/^HOOKS=/s/ autodetect / /' /etc/mkinitcpio.conf
		mkinitcpio -p linux

		echo 'Prevent the systemd journal service from writing to the USB by configuring it to use RAM.'
		#https://mags.zone/help/arch-usb.html#journal
		mkdir -p /etc/systemd/journald.conf.d
		cat >/etc/systemd/journald.conf.d/10-volatile.conf <<EOIF
[Journal]
Storage=volatile
SystemMaxUse=16M
RuntimeMaxUse=32M
EOIF

		echo "Don't save access time to reduce writes"
		#https://mags.zone/help/arch-usb.html#noatime
		sed -i '1i TODO: add noatime to options to prevent saving access times to reduce writes\n' /etc/fstab
		vim /etc/fstab
	fi

	echo 'Boot loader'
	grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
	cp /etc/default/grub /etc/default/grub.bak
	sed -Ei 's/(GRUB_TIMEOUT)=[0-9]+/\1=0/' /etc/default/grub
	if [ "$ENCRYPT" = "y" ]; then
		CRYPTDEVICE_UUID="`lsblk -dno uuid $PART_ENCRYPTED`"
		if [ "\$UDEV" = "y" ]; then
			KERNEL_PARAMS="cryptdevice=UUID=\$CRYPTDEVICE_UUID:$NAME_DECRYPTED root=$PART_ROOT"
		else
			KERNEL_PARAMS="rd.luks.name=\$CRYPTDEVICE_UUID=$NAME_DECRYPTED root=$PART_ROOT"
		fi
		sed -Ei 's#^(GRUB_CMDLINE_LINUX_DEFAULT=".*)(")#\1 '"\$KERNEL_PARAMS"'\2#' /etc/default/grub
	fi
	grub-mkconfig -o /boot/grub/grub.cfg
EOF

	chmod a+x "$script"
	if [ "$ECHO" = "y" ]; then
		echo "running $script in chroot"
	fi
	run arch-chroot /mnt "${script#/mnt}"
}

runaschroot

if [ -f "2-post-install.sh" ]; then
	run cp "2-post-install.sh" "ui.sh" "settings2" /mnt/root
	chmod a+x /mnt/root/2-post-install.sh
	# /root cannot be accessed by normal user
	mkdir /mnt/etc/install
	if [ -d "dot_password-store" -a -d "dot_gnupg" ]; then
		run cp -r "dot_password-store" "dot_gnupg" /mnt/etc/install
	fi
	if [ -d "dotfiles" ]; then
		run cp -r "dotfiles" /mnt/etc/install
	fi
fi

echo
echo "Done."
echo "I will poweroff in 10 seconds."
echo "Unplug the usb stick, boot into your new system and run"
echo "./2-post-install.sh"
sleep 10
poweroff
