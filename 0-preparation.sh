#!/usr/bin/env bash

# bash strict mode
set -euo pipefail
IFS="$(printf '\n\t')"

. ui.sh


usage() {
echo "\
Usage: $0 [options] [device]

Options:
  -d, --download  download the image
  -w, --write     do both --dd and --cp
      --dd        write the image to the device
      --cp        copy the installation script to the device
  -p, --passwords copy all your passwords stored in pass
                  and all your gpg keys if --cp is done
  -n, --no-passwords
                  don't copy passwords or gpg keys
  -c, --dotfiles PATH
                  a colon separated list of paths to dotfile
                  directories to be copied if --cp is done
                  empty string: don't copy or create dotfile directories
                  'new': don't copy dotfile directories but init new repos
                  (default: ~/.dotfiles and ~/.dotfiles-private if -p)
  -h, --help      show this help and exit

If no options are given both --download and --write are assumed.

If no device is given a list of existing devices is shown
and you are prompted to insert the device path you want to write to.
"
}


PARSED_ARGUMENTS=$(getopt -n "$0" -o hdwpnc: --long help,download,write,dd,cp,passwords,no-passwords,dotfiles: -- "$@")
VALID_ARGUMENTS=$?

DEFAULTVAL=y
DO_DOWNLOAD=
DO_DD=
DO_CP=
DO_CP_PASS=
DOTFILES="<default>"

eval set -- "$PARSED_ARGUMENTS"
while true; do
	case "$1" in
		-h | --help)     usage; exit ;;
		-d | --download) DEFAULTVAL=n; DO_DOWNLOAD=Y;    shift ;;
		-w | --write)    DEFAULTVAL=n; DO_DD=y; DO_CP=y; shift ;;
		     --dd)       DEFAULTVAL=n; DO_DD=y;          shift ;;
		     --cp)       DEFAULTVAL=n; DO_CP=y;          shift ;;
		-p | --passwords)              DO_CP_PASS=y;     shift ;;
		-n | --no-passwords)           DO_CP_PASS=n;     shift ;;
		-c | --dotfiles)               DOTFILES="$2";    shift 2 ;;
		# -- means the end of the arguments; drop this, and break out of the while loop
		--) shift; break ;;
		# If invalid options were passed, then getopt should have reported an error,
		# which we checked as VALID_ARGUMENTS when getopt was called...
		*) echo "Unexpected option: $1" ;;
	esac
done

if [ "$VALID_ARGUMENTS" != "0" ]; then
	usage
	exit 1
fi

DO_DOWNLOAD="${DO_DOWNLOAD:-$DEFAULTVAL}"
DO_DD="${DO_DD:-$DEFAULTVAL}"
DO_CP="${DO_CP:-$DEFAULTVAL}"
if [ "$DO_DOWNLOAD" != "n" -o "$DO_DD" != "n" ]; then
	DO_CHECK="y"
else
	DO_CHECK="n"
fi
if [ "$DO_DD" = "y" -o "$DO_CP" = "y" ]; then
	NEEDS_OUTPUT_DEVICE="y"
else
	NEEDS_OUTPUT_DEVICE="n"
fi
if [ "$DO_CP" = "y" -a "$DO_CP_PASS" = "" ]; then
	if ask_yes_no "do you want to copy your passwords saved in pass and gpg keys?"; then
		DO_CP_PASS=y
	else
		DO_CP_PASS=n
	fi
fi
if [ "$DOTFILES" = "<default>" ]; then
	DOTFILES=""
	if [ -e "$HOME/.dotfiles" ]; then
		DOTFILES="$DOTFILES${DOTFILES:+:}$HOME/.dotfiles"
	fi
	if [ "$DO_CP_PASS" = "y" -a -e "$HOME/.dotfiles-private" ]; then
		DOTFILES="$DOTFILES${DOTFILES:+:}$HOME/.dotfiles-private"
	fi
fi

echo "DO_DOWNLOAD: $DO_DOWNLOAD"
echo "DO_DD      : $DO_DD "
echo "DO_CP      : $DO_CP"
echo "DO_CP_PASS : $DO_CP_PASS"
echo "DOTFILES   : $DOTFILES"


# ========== utils =========

is_installed() { which "$1" >/dev/null; }
is_in_group() { id -nG "`whoami`" | grep -qw "$1"; }


# ========== global variables =========

iso_file="$(find . -maxdepth 1 -iname '*arch*.iso' -type f,l -printf '%P\n' | sort | tail -1)"
md5sum_file="md5sums.txt"
sha256sum_file="sha256sums.txt"
next_script="1-setup.sh"
files_to_copy=("ui.sh" "rm-passwords.sh" "$next_script" "get_unused_device.py" "settings" "2-post-install.sh" "settings2")

output_device="${1-}"
settings_partition=3

if [ $# -gt 1 ]; then
	echo >&2 "too many arguments given"
	usage
	exit 1
fi

echo "iso_file   : $iso_file"


# ========== save settings =========

ENCRYPT=y

cat >settings <<eof
KEYMAP='$(sed -n 's/KEYMAP=//p' /etc/vconsole.conf 2>/dev/null)'
TIMEZONE='`realpath -e /etc/localtime 2>/dev/null`'
LOCALE='$LANG'
HOSTNAME=''
LOCALDOMAIN='mydomain'

ENCRYPT=$ENCRYPT
WIPE=\$ENCRYPT
CRYPTSETUP=
#BENCHMARK=y

DEV=AUTO
PART_SIZE_BOOT=300MiB
PART_SIZE_ROOT=20GiB
PART_SIZE_VAR=10GiB
PART_SIZE_SWAP=auto
PART_SIZE_FREE=50%

UDEV=auto
eof

cat >settings2 <<eof
USERNAME=
AUTOLOGIN=$ENCRYPT
SWAY=y
X11=n
GNOME=n
XKBMAP="$(setxkbmap -v 10 | sed -En 's/^(layout|variant|options):\s+/-\1 /p' | sed 's/-options/-option/' | tr '\n' ' ')"
JAPANESE_INPUT=y
LAPTOP=y
USER_SOFTWARE=y
DEVELOPMENT_TOOLS=y
RUST=y
NEOVIM=y
LATEX=y
NEOMUTT=y
PASS=y
AUR=y
YAY=y
SUDO=n
COPY_PASSWORDS=${DO_CP_PASS}
# if NEW_PASS_KEY is empty and COPY_PASSWORDS is y reuse old key intead of creating a new one
NEW_PASS_KEY=pass
WORK=n
eof


# ========== choose and check output device =========

print_available_devices() {
	echo
	echo "available devices:"
	lsblk -dn -o path,size,model
}

read_output_device() {
	echo -n "which device do you want to write to? "
	read -r output_device
}

is_valid_device() {
	lsblk -dn -o path | grep "^$1$"
}

assert_device_not_mounted() {
	local device mounted
	device="$1"
	mounted="$(lsblk -o path,mountpoint -n --pairs "$device" | sed -En '/MOUNTPOINT="[^"]/p' | sed -E 's/.*PATH="([^"]+)".*/\1/')"
	if [ "$mounted" != "" ]; then
		echo "ERROR: The following partitions are mounted. Please unmount them before proceeding:"
		echo "$mounted"
		exit 1
	fi
}

if [ "$NEEDS_OUTPUT_DEVICE" = "y" ]; then
	if [ "$output_device" = "" ]; then
		print_available_devices
		read_output_device
	fi
	while ! is_valid_device "$output_device"; do
		echo
		echo "$output_device is not a valid device"
		print_available_devices
		read_output_device
	done

	while ! [ -b "$output_device" ]; do
		echo
		echo "$output_device is not an existing device."
		print_available_devices
		read_output_device
	done

	assert_device_not_mounted "$output_device"
fi


# ========== download iso if it does not exist =========

iso_exits() {
	#ls -1 | grep '.*arch.*\.iso$' >/dev/null
	[ "$iso_file" != "" ]
}

download_iso() {
	local url='https://dist-mirror.fem.tu-ilmenau.de/archlinux/iso/latest/'
	local fail=false
	local fntmp="$(mktemp)"
	curl "$url" 2>/dev/null >"$fntmp"
	iso_file="$(sed -En 's:\s*<a href="(\./)?(archlinux.*\.iso)">:\2:p' "$fntmp" | head -n1)"
	if [ "$iso_file" = "" ]; then
		echo "Failed to retrieve the file name to download."
		fail=true
	elif ! ( set -x; curl -o "$iso_file" "$url$iso_file" ); then
		echo "Failed to download $url$iso_file"
		fail=true
	elif ! ( set -x; curl -o "$md5sum_file" "$url$md5sum_file" ); then
		:
	fi

	if [ ! -s "$md5sum_file" ]; then
		# md5sum download failed, retry with sha256
		if ! ( set -x; curl -o "$sha256sum_file" "$url$sha256sum_file" ); then
			echo "Failed to download $url$md5sum_file"
			echo "Failed to download $url$sha256sum_file"
			fail=true
		elif [ ! -s "$sha256sum_file" ]; then
			echo "Failed to download $url$md5sum_file"
			echo "Failed to download $url$sha256sum_file"
			fail=true
		fi
	fi

	if [ "$fail" = "true" ]; then
		echo "
Please download the iso file and md5sums.txt from
https://archlinux.org/download/
$url
and run this script again from the directory where the iso file is located.

The html code from $url is saved in
$fntmp"
		exit 0
	fi
	rm "$fntmp"
}

if [ "$DO_DOWNLOAD" = "Y" ]; then
	download_iso
elif [ "$DO_DOWNLOAD" = "y" ]; then
	if ! iso_exits; then
		download_iso
	else
		echo "iso file found: $iso_file"
	fi
fi


# ========== check image =========

check_md5_sum() {
	if [ -s "$md5sum_file" ]; then
		md5sum -c <(grep "$iso_file$" "$md5sum_file")
	elif [ -s "$sha256sum_file" ]; then
		sha256sum -c <(grep "$iso_file$" "$sha256sum_file")
	else
		echo "missing $md5sum_file or $sha256sum_file"
		exit 1
	fi
}

if [ "$DO_CHECK" = "y" ]; then
	check_md5_sum || exit 1
fi


# ========== write to usb =========

settings_partition_exists() {
	lsblk "$output_device" -o path -n | grep "$output_device$settings_partition" >/dev/null
}

write() {
	local script
	local cwd
	script="`mktemp`"
	cwd="`pwd`"
	cat >"$script" <<EOF
	set -euo pipefail
	cd "$cwd"

EOF

	if [ "$DO_DD" = "y" ]; then
		cat >>"$script" <<EOF
		echo -n "data to write: "; du -hL '$iso_file'
		dd if='$iso_file' of='$output_device' status=progress
		sync
		partprobe
		sleep 1
		echo "finished writing $iso_file to $output_device"

EOF
	fi

	if [ "$DO_CP" = "y" ] && ! settings_partition_exists || [ "$DO_DD" = "y" ]; then
		local required_size
		#required_size=$(du -ck settings 1-setup.sh | tail -1 | sed -E 's/\s+.*$//')
		#required_size=$(( $required_size * 2 ))
		#required_size=${required_size}K
		required_size=100M
		cat >>"$script" <<EOF
		echo 'creating new settings partition: $output_device$settings_partition'
		echo 'size=$required_size;' | sfdisk --append "$output_device"
		sync
		partprobe
		sleep 1
		echo 'creating file system on new settings partition'
		mkfs.ext4 '$output_device$settings_partition'

		echo 'setting write permisson on new settings partition'
		mountpoint="\$(mktemp -d)"
		mount '$output_device$settings_partition' "\$mountpoint"
		chmod 777 "\$mountpoint"
		umount "\$mountpoint"
		echo 'finished creating new settings partition'

EOF
	fi

	cat "$script"
	if is_installed sudo && is_in_group sudo; then
		sudo bash "$script"
	else
		echo "please enter the root password"
		su - -c "bash '$script'"
	fi
	rm "$script"
}

cponly() {
	local mountdir
	mountdir="$(udisksctl mount -b "$output_device$settings_partition" | sed 's/^.* at //')"
	rm -rf "$mountdir"/*
	cpdotfiles
	cp "${files_to_copy[@]}" "$mountdir"
	chmod a+x "$mountdir/$next_script"
	if [ "$DO_CP_PASS" = 'y' ]; then
		cp -r "${PASSWORD_STORE_DIR:-$HOME/.password-store}" "$mountdir/dot_password-store"
		cp -r "$HOME/.gnupg" "$mountdir/dot_gnupg"
	fi
	udisksctl unmount -b "$output_device$settings_partition"
}

cpdotfiles() {
	local IFS newname
	if [ "$DOTFILES" == "" ]; then
		echo 'DOTFILES=' >>settings2
		return
	fi
	if [ "$DOTFILES" == "new" ]; then
		echo 'DOTFILES=new' >>settings2
		return
	fi
	
	mkdir "$mountdir/dotfiles"
	IFS=:
	for path in $DOTFILES; do
		newname="${path##*/}"
		newname="dot_${newname#.}"
		cp -r "$path" "$mountdir/dotfiles/$newname"
	done
	# inner command substitution to strip trailing newlines from ls
	#https://stackoverflow.com/questions/12524308/bash-strip-trailing-linebreak-from-output/12524345#12524345
	echo "DOTFILES=$(echo -n "$(ls -1 --indicator-style=none "$mountdir/dotfiles")" | tr '\n' :)" >>settings2
}

if [ "$NEEDS_OUTPUT_DEVICE" = "y" ]; then
	if [ "$DO_DD" != "n" ] || ! settings_partition_exists; then
		write
	fi
	if [ "$DO_CP" = "y" ]; then
		cponly
	fi
	echo 'This script is finished. You can now plug out the USB stick.'
fi

if [ "$DO_CP" = "y" ]; then
	echo 'Boot the live stick and run the following commands:'
	echo '# loadkeys de'
	echo '# dd if=/dev/sdb3 of=install.img'
	echo '# mkdir install'
	echo '# mount install.img install'
	echo '# cd install'
	echo "# ./$next_script"
	echo ''
	echo 'If you have no internet despite a cable being connected try'
	echo '# dhclient'
fi
