#!/usr/bin/env bash

# run this script on the arch install stick before 1-setup.sh
# if you don't want to install passwords

rm -r dot_gnupg
rm -r dot_password-store
rm -r dotfiles/dot_dotfiles-private
sed -i '/^DOTFILES=/ s/:dot_dotfiles-private//' settings2
