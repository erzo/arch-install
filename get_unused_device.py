#!/usr/bin/env python3

'''
Print unused device on stdout and ask which one to print on stderr if there are several unused devices.
'''

import sys
import subprocess
import json
from collections.abc import Mapping, Iterator

def assert_str(s: object) -> str:
	assert isinstance(s, str)
	return s

def assert_optional_str(s: object) -> 'str|None':
	assert s is None or isinstance(s, str)
	return s

def to_str(s: object) -> str:
	if s is None:
		return ""
	return str(s)


class Device:

	def __init__(self, d: 'Mapping[str, object]') -> None:
		self.path = assert_str(d['path'])
		self.mountpoint = assert_optional_str(d['mountpoint'])
		self.model = to_str(d['model'])
		self.size = to_str(d['size'])
		self.in_use = bool(self.mountpoint or d.get('children', []))
		self.partitions: 'list[Device]' = []

	def __repr__(self) -> str:
		return f"{type(self).__name__}(path={self.path!r}, mountpoint={self.mountpoint!r})"

def get_devices() -> 'Iterator[Device]':
	completed_process = subprocess.run(['lsblk', '--json', '--tree', '--output=path,mountpoint,model,size'], check=True, capture_output=True, text=True)
	devices = json.loads(completed_process.stdout)['blockdevices']
	for d in devices:
		dev = Device(d)
		for p in d.get('children', []):
			dev.partitions.append(Device(p))
		yield dev

def get_unused_devices() -> 'Iterator[Device]':
	for dev in get_devices():
		if not dev.mountpoint and not any(partition.in_use for partition in dev.partitions):
			yield dev

def main() -> None:
	'''
	Find all unused devices and ask which one to choose if there are several.
	Print result on stdout, print questions on stderr.
	'''
	devices = list(get_unused_devices())
	if not devices:
		print("No unused device.", file=sys.stderr)
		return

	elif len(devices) == 1:
		output_device = devices[0]

	else:
		width_path = max(len(dev.path) for dev in devices)
		width_size = max(len(dev.size) for dev in devices)
		while True:
			print("There are several unused devices. Please enter which one you want to use:", file=sys.stderr)
			for i, dev in enumerate(devices):
				print(f"{i}: {dev.path:{width_path}}  {dev.size:{width_size}} {dev.model}", file=sys.stderr)
			print(">>> ", end="", file=sys.stderr)
			ln = input().strip()
			if not ln:
				continue

			try:
				i = int(ln)
				output_device = devices[i]
				break
			except ValueError:
				pass
			except IndexError:
				print("Invalid index", file=sys.stderr)
				continue

			chosen_devices = [dev for dev in devices if ln in dev.path]
			n = len(chosen_devices)
			if n == 0:
				print("Invalid path", file=sys.stderr)
				continue
			else:
				devices = chosen_devices

			if n == 1:
				output_device = devices[0]
				break
			else:
				print("Input is ambiguous")
				continue


	print(output_device.path)


if __name__ == '__main__':
	main()
